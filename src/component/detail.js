import React from 'react'

const Detail = props => <div>
    <span className='detailName'>{props.name}</span>
    <span>{props.detail}</span>
</div>

export default Detail
