import React from 'react'
import Paper from 'material-ui/Paper';

const Column = props => {
    return  <Paper zDepth={2} >
        <div onClick={() => props.selectCountry(props.name)} className='column' style={props.style}>
            <img src={ props.imgLink } /> {props.name}
            <br />
        </div>
    </Paper>
}

export default Column
