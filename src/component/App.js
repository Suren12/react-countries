import _ from 'lodash'
import axios from 'axios'
import React , { Component } from 'react'

import Search from './Search'
import Column from './column'
import Details from './details'

class App extends Component {
    state = {
        data: [],
        selectedCountry: {},
        searchText: ''
    }

    componentDidMount () {
        axios.get('https://restcountries.eu/rest/v2/all').then(data => {
            this.setState({data: data.data})
        })
    }

    selectCountry = (name) => {
        const selectedCountry = this.state.data.find(country => country.name === name)
        this.setState({selectedCountry})
    }

    setSearchText = searchText => this.setState({searchText})

    renderCountries = () => this.state.data.filter(country =>
        country.name.toLowerCase().includes(this.state.searchText.toLowerCase())).map(country => <Column
        style={this.highlightSelectedCountry(country.name)}
        selectCountry={this.selectCountry}
        key={country.name}
        name={country.name}
        imgLink={country.flag} />)

    renderDetails = () => {
        if(_.isEmpty(this.state.selectedCountry)) {
            return <div></div>
        }
        return <Details country={this.state.selectedCountry} />
    }

    highlightSelectedCountry = (name) => {
        if(this.state.selectedCountry.name === name) {
            return ({borderColor: 'red'})
        }
        return {}
    }

    render () {
        return (
            <div>
                <Search value={this.state.searchText} onSearch={this.setSearchText} />
                <div style={{display: 'flex'}}>
                    <div className='container'>
                        {this.renderCountries()}
                    </div>
                    {this.renderDetails()}
                </div>
            </div>
        )
    }
}

export default App
