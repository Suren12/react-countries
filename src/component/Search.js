import React from 'react'

import TextField from 'material-ui/TextField';

const Search = props => {
    let myTextElem = null
    return (
        <div>
            <TextField
                value={props.value} type='text'
                onChange={e => props.onSearch(e.target.value)}
                id="text-field-default"
                hintText="Search Countries"
            />
        </div>
    )
}

export default Search
