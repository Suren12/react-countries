import React from 'react'
import Detail from './detail'
import Paper from 'material-ui/Paper';

const style = {
    padding: '10px'
};

const Details = ({country: {alpha2Code, area, capital}}) => {
    return (
        <div className='Details' style={{position: 'fixed' , right: '50%'}}>
            <Paper zDepth={2} style={style}>
                <Detail name='CODE: ' detail={alpha2Code} />
                <Detail name='AREA: ' detail={area} />
                <Detail name='CAPITAL: ' detail={capital} />
            </Paper>
        </div>
    )
}

export default Details
